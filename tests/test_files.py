import unittest

from app import app
from app.config import DEFAULT_FILE_NAME


class TestBookingService(unittest.TestCase):
    def setUp(self):
        self.url = "http://127.0.0.1:5000"
        self.client = app.test_client()

    def test_home(self):
        response = self.client.get("{}/".format(self.url))

        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()

        self.assertIn('file_read', response_json)
        self.assertEqual(response_json['file_read'], DEFAULT_FILE_NAME)

        self.assertIn('lines', response_json)
        self.assertEqual(len(response_json['lines']), 150)

    def test_file_selecting(self):
        file_name = 'file2.txt'
        response = self.client.get("{}/{}".format(self.url, file_name))
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(response_json['file_read'], file_name)

    def test_file_rows_subset(self):
        file_name = 'file2.txt'
        read_from = 10
        read_to = 20
        expected_lines_count = read_to - read_from + 1
        url = "{}/{}/{}/{}".format(self.url, file_name, read_from, read_to)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response_json = response.get_json()
        self.assertEqual(len(response_json['lines']), expected_lines_count)

    def test_wrong_endpoint(self):
        response = self.client.get("{}/books/book1.txt".format(self.url))
        self.assertEqual(response.status_code, 404)

    def test_wrong_filename(self):
        response = self.client.get("{}/book1.txt".format(self.url))
        self.assertEqual(response.status_code, 404)

    def test_wrong_range(self):
        file_name = 'file2.txt'
        url = "{}/{}/{}/{}".format(self.url, file_name, 20, 10)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


if __name__ == "__main__":
    unittest.main()
