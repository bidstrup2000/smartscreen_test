import json
import os

from flask import make_response, request


def root_dir():
    """ Returns root director for this project """
    return os.path.dirname(os.path.realpath(__file__ + '/..'))


def make_json_response(arg, status=200):
    response = make_response(json.dumps(arg, indent=4), status)
    response.headers['Content-type'] = "application/json"
    return response


def clean_line(line):
    """ Returns only string """
    return ':'.join(line.split(':')[1:]).strip()


def create_list_of_string(file_name, read_from, read_to):
    """ Creates list of string"""
    lines_list = []
    lines_count = 0
    with open(file_name, 'r') as f:
        for k, l in enumerate(f.readlines(), 1):
            # if we need only subset
            if k < read_from or (read_to and k > read_to):
                continue

            line_cleaned = clean_line(l)
            lines_list.append(line_cleaned)
            lines_count += 1

    return lines_count, lines_list


def validate_user_data(full_file_path, read_from, read_to):
    # file doesnt exist
    if not os.path.isfile(full_file_path):
        data = {
            'error': 'File "{}" does not exist'.format(request.url),
        }
        return make_json_response(data, 404)

    # range error
    if read_from and read_to and (read_from > read_to):
        data = {
            'error': 'Wrong range. "end line" must be greater than "start line"'.format(request.url),
        }
        return make_json_response(data, 404)
