import os

from flask import request

from app import app
from app.config import DEFAULT_FILE_NAME
from app.utils import make_json_response, root_dir, create_list_of_string, validate_user_data


@app.route('/', defaults={'file_name': DEFAULT_FILE_NAME, 'read_from': 1, 'read_to': None})
@app.route('/<string:file_name>', defaults={'read_from': 1, 'read_to': None})
@app.route('/<string:file_name>/<int:read_from>/<int:read_to>')
@app.route('/<int:read_from>/<int:read_to>', defaults={'file_name': DEFAULT_FILE_NAME})
def index(file_name, read_from, read_to):
    full_file_path = os.path.join(root_dir(), 'data', file_name)

    # uri validating
    error = validate_user_data(full_file_path, read_from, read_to)
    if error:
        return error

    # response creating
    number_of_lines, lines = create_list_of_string(full_file_path, read_from, read_to)
    data = {
        'file_read': file_name,
        'number_of_lines_read': number_of_lines,
        'lines': lines,
    }
    return make_json_response(data)


@app.errorhandler(404)
def page_not_found(e):
    data = {
        'error': 'Resource "{}" does not exist'.format(request.url),
    }
    return make_json_response(data, 404)
